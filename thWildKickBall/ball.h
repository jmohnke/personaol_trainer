/**
 * @abstract Basic module for kicking a ball
 * @author Stefan Wirth
 */

#ifndef BALL_BALL_H
#define BALL_BALL_H

#include <boost/shared_ptr.hpp>
#include <alcommon/almodule.h>
#include <string>

#include <alproxies/almemoryproxy.h>
#include <alproxies/almotionproxy.h>
#include <alproxies/alrobotpostureproxy.h>
#include <alproxies/altexttospeechproxy.h>
#include <althread/almutex.h>

namespace AL
{
  class ALBroker;
}

class Ball : public AL::ALModule
{
  public:

    Ball(boost::shared_ptr<AL::ALBroker> broker, const std::string& name);

    virtual ~Ball();

    /** Overloading ALModule::init().
    * This is called right after the module has been loaded
    */
    virtual void init();

    /**
        @abstract Called when the back of the right hand is touched
    */
    void onHandRightBackTouched();

  private:
    AL::ALMemoryProxy fMemoryProxy;
    AL::ALTextToSpeechProxy fTtsProxy;
    AL::ALMotionProxy fMotionProxy;
    AL::ALRobotPostureProxy fPostureProxy;
    //to prevent double callbacks
    bool isKicking;

    boost::shared_ptr<AL::ALMutex> fCallbackMutex;

    /**
        @abstract Initialize kicking position
    */
    void initKickPosition();

    /**
        @abstract Perform kicking movement
    */
    void performKick();
    /**
         @abstract Calculate the path for the movement
         @return Array with 3 6D-Vectors
    */
    AL::ALValue calculatePath();
    /**
        @abstract Return to the initial position before kicking
    */
    void returnToInitialPosition();
    /**
        @abstract Cheer for ChristiNAO
    */
    void cheer();
};

#endif  // BALL_BALL_H
