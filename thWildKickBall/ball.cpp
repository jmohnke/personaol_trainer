/**
 * @abstract Basic module for kicking a ball
 * @author Stefan Wirth
 */

#include "ball.h"

#include <alvalue/alvalue.h>
#include <alcommon/alproxy.h>
#include <alcommon/albroker.h>
#include <qi/log.hpp>
#include <althread/alcriticalsection.h>

//Constants for body movement
std::string RIGHT_LEG_IDENTIFIER = "RLeg";
std::string LEFT_LEG_IDENTIFIER = "LLeg";
std::string LEGS_IDENTIFIER = "Legs";
std::string FIXED_IDENTIFIER = "Fixed";
std::string FREE_IDENTIFIER = "Free";
std::string STAND_INIT_IDENTIFIER = "StandInit";

//Constants for path calculation
float D_X = 0.05;
float D_Z = 0.05;
float D_WY = 5.0 * (std::atan(1) * 4);

int CALCULATION_AXIS_MASK = 63;
int CALCULATION_FRAME = 1;

/**
 * The constructor which sets up the module with its description and methods.
 */
Ball::Ball(
  boost::shared_ptr<AL::ALBroker> broker,
  const std::string& name): AL::ALModule(broker, name),
    fCallbackMutex(AL::ALMutex::createALMutex())
{
  setModuleDescription("This module uses the middle tactile of the head to trigger a kick of a ball");

  functionName("onHandRightBackTouched", getName(), "Method called when the back of the right hand is touched");
  BIND_METHOD(Ball::onHandRightBackTouched);
}

/**
 * The method being called when unloading the module.
 */
Ball::~Ball() {
    // unsubscribe the callback methods
  fMemoryProxy.unsubscribeToEvent("onHandRightBackTouched", "Ball");
}

/**
  * The method being called when loading the module.
  */
void Ball::init() {
  try {

    //bacis init and event subscriptions
    fMemoryProxy = AL::ALMemoryProxy(getParentBroker());
    fTtsProxy = AL::ALTextToSpeechProxy(getParentBroker());
    fMotionProxy = AL::ALMotionProxy(getParentBroker());
    fPostureProxy = AL::ALRobotPostureProxy(getParentBroker());
    isKicking = false;

    fMemoryProxy.subscribeToEvent(
        "HandRightBackTouched",
        "Ball",
        "onHandRightBackTouched"
    );
  }
  catch (const AL::ALError& e) {
    qiLogError("module.example") << e.what() << std::endl;
  }
}

void Ball::onHandRightBackTouched() {
    if(isKicking) {
        return;
    }

    isKicking = true;

    AL::ALCriticalSection section(fCallbackMutex);

    initKickPosition();

    performKick();

    returnToInitialPosition();

    cheer();

    fMotionProxy.rest();
    isKicking = false;
}

void Ball::initKickPosition() {
    double timeToSwitchBalance = 2.1;
    double speedToInitStandPosture = 0.6;
    fMotionProxy.wakeUp();
    fPostureProxy.goToPosture(STAND_INIT_IDENTIFIER, speedToInitStandPosture);
    fMotionProxy.wbEnable(true);
    fMotionProxy.wbFootState(FIXED_IDENTIFIER, LEGS_IDENTIFIER);
    fMotionProxy.wbEnableBalanceConstraint(true, LEGS_IDENTIFIER);
    fMotionProxy.wbGoToBalance(LEFT_LEG_IDENTIFIER, timeToSwitchBalance);
    fMotionProxy.wbFootState(FREE_IDENTIFIER, RIGHT_LEG_IDENTIFIER);
}

void Ball::performKick() {
    AL::ALValue interpolationTimes = AL::ALValue::array(1.0, 1.4, 2.1);
    AL::ALValue motionPath = calculatePath();

    fMotionProxy.positionInterpolation(
        RIGHT_LEG_IDENTIFIER,
        CALCULATION_FRAME,
        motionPath,
        CALCULATION_AXIS_MASK,
        interpolationTimes,
        false
    );
}

AL::ALValue Ball::calculatePath() {
    AL::ALValue legBackTransform = AL::ALValue::array(-0.7 * D_X, 0.0, 1.1 * D_Z, 0.0, +D_WY, 0.0);
    AL::ALValue kickTransform = AL::ALValue::array(2.2 * D_X, +D_X, D_Z, 0.0, -D_WY, 0.0);
    AL::ALValue returnToInitialTransform =  AL::ALValue::array(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

    return AL::ALValue::array(legBackTransform, kickTransform, returnToInitialTransform);
}

void Ball::returnToInitialPosition() {
    fMotionProxy.wbEnableEffectorOptimization(RIGHT_LEG_IDENTIFIER, false);
    sleep(1);
    fMotionProxy.wbEnable(false);
    fPostureProxy.goToPosture(STAND_INIT_IDENTIFIER, 0.3);
}

void Ball::cheer() {
    std::string language = "German";
    fTtsProxy.say("Tor", language);
    fTtsProxy.say("ChristiNAO Ronaldo ist der beste",language);
}
