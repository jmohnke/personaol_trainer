/*
 * Copyright (c) 2012, 2013 Aldebaran Robotics. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the COPYING file.
 */



    /**
    * The helloJoints example shows a way of how to control the robot's joints.
    * Additional information about this part:
    * http://www.aldebaran-robotics.com/documentation/naoqi/motion/control-joint.html
    */

#include <boost/signals2.hpp>
#include <boost/shared_ptr.hpp>
#include <alcommon/albroker.h>
#include <alcommon/almodule.h>
#include <alcommon/albrokermanager.h>
#include <alcommon/altoolsmain.h>

    #include "climbStairs.h"

    # define ALCALL
#define BOOST_SIGNALS_NO_DEPRECATION_WARNING


extern "C"
{
  ALCALL int _createModule(boost::shared_ptr<AL::ALBroker> pBroker)
  {
    // init broker with the main broker instance
    // from the parent executable
    AL::ALBrokerManager::setInstance(pBroker->fBrokerManager.lock());
    AL::ALBrokerManager::getInstance()->addBroker(pBroker);
    AL::ALModule::createModule<climbStairs>(pBroker, "climbStairs" );

    return 0;
  }

  ALCALL int _closeModule()
  {
    return 0;
  }
}
