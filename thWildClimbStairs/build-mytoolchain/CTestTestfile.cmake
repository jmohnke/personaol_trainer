# CMake generated Testfile for 
# Source directory: /home/nao/helloNao/walkStairs
# Build directory: /home/nao/helloNao/walkStairs/build-mytoolchain
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(test_walkStairs "/home/nao/helloNao/walkStairs/build-mytoolchain/sdk/bin/test_walkStairs")
SET_TESTS_PROPERTIES(test_walkStairs PROPERTIES  TIMEOUT "20")
