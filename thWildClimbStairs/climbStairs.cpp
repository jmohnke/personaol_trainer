#include "climbStairs.h"

#include <alvalue/alvalue.h>
#include <alcommon/alproxy.h>
#include <alcommon/albroker.h>
#include <qi/log.hpp>
#include <althread/alcriticalsection.h>

climbStairs::climbStairs(
        //This code in the constructor is needed for the creation of the local module
    boost::shared_ptr<AL::ALBroker> broker,
    const std::string& name): AL::ALModule(broker, name),
   fCallbackMutex(AL::ALMutex::createALMutex()){
        setModuleDescription("This module lets the NAO climb 3 Stair steps");

        functionName("onRearTactilTouched", getName(), "Method called when the right hand is pressed. Lets the NAO climb the Stairs");
        //here the function is bound on the event
        BIND_METHOD(climbStairs::onRearTactilTouched);
    }
//here the function gets unbound
climbStairs::~climbStairs(){
    fMemoryProxy.unsubscribeToEvent("onRearTactilTouched", "climbStairs");
}

void climbStairs::init(){
    try {
        //initialise the flag
        flag = true;
        //create proxys from parentbroker
        fMemoryProxy = AL::ALMemoryProxy(getParentBroker());
        motion = AL::ALMotionProxy(getParentBroker());
        //subscription of the event of the touch happens here
        fMemoryProxy.subscribeToEvent("RearTactilTouched", "climbStairs",
                                      "onRearTactilTouched");
      }
      catch (const AL::ALError& e) {
        qiLogError("module.example") << e.what() << std::endl;
      }
}


void climbStairs::onRearTactilTouched(){
    //this secures that my function only gets called once per press
    if (flag == true){
        flag = false;

     qiLogInfo("module.example") << "Executing callback method on right bumper event" << std::endl;
        //this is necessary for the prevention of problems with the access of critical sections
      AL::ALCriticalSection section(fCallbackMutex);

    //tells the NAO to  call out what he will be doing
      AL::ALTextToSpeechProxy text = AL::ALTextToSpeechProxy(getParentBroker());
      text.say("Climbing all the stairs");
      //Enable stiffness and lets the nao stand up
      motion.wakeUp();
       // go to start position(necesarry for angleintermpolation steps)
      climbStairs::goToStartPosition(motion);
      //walk 1 stair 3 times
      climbStairs::takeOneStair(motion);
      climbStairs::takeOneStair(motion);
      climbStairs::takeOneStair(motion);
        //go to crouch and set stiffnes to 0
      motion.rest();
      //end the lock of the flag
    flag = true;
    }
}
//interpolates between joint angles to let the nao do an exact choreografic move to climb exactly one stair
void climbStairs::takeOneStair(AL::ALMotionProxy motion)
    {
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(14);
    keys[0].arraySetSize(14);

    times[0][0] = 0.5;
    keys[0][0] = AL::ALValue::array(-0.20253, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[0][1] = 2.5;
    keys[0][1] = AL::ALValue::array(-0.20253, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[0][2] = 4;
    keys[0][2] = AL::ALValue::array(-0.22554, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[0][3] = 5.5;
    keys[0][3] = AL::ALValue::array(-0.208666, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[0][4] = 8.5;
    keys[0][4] = AL::ALValue::array(-0.224006, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[0][5] = 10;
    keys[0][5] = AL::ALValue::array(-0.176453, AL::ALValue::array(3, -0.5, -0.012272), AL::ALValue::array(3, 0.5, 0.012272));
    times[0][6] = 11.5;
    keys[0][6] = AL::ALValue::array(-0.150374, AL::ALValue::array(3, -0.5, -0.0079258), AL::ALValue::array(3, 0.5, 0.0079258));
    times[0][7] = 13;
    keys[0][7] = AL::ALValue::array(-0.128898, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[0][8] = 14.5;
    keys[0][8] = AL::ALValue::array(-0.170316, AL::ALValue::array(3, -0.5, 0.0102267), AL::ALValue::array(3, 0.5, -0.0102267));
    times[0][9] = 16;
    keys[0][9] = AL::ALValue::array(-0.190258, AL::ALValue::array(3, -0.5, 0.00664741), AL::ALValue::array(3, 0.5, -0.00664741));
    times[0][10] = 17.5;
    keys[0][10] = AL::ALValue::array(-0.2102, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[0][11] = 19;
    keys[0][11] = AL::ALValue::array(-0.190258, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.833333, 0));
    times[0][12] = 21.5;
    keys[0][12] = AL::ALValue::array(-0.205598, AL::ALValue::array(3, -0.833333, 0), AL::ALValue::array(3, 0.5, 0));
    times[0][13] = 23;
    keys[0][13] = AL::ALValue::array(-0.20253, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(14);
    keys[1].arraySetSize(14);

    times[1][0] = 0.5;
    keys[1][0] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[1][1] = 2.5;
    keys[1][1] = AL::ALValue::array(-0.0123138, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[1][2] = 4;
    keys[1][2] = AL::ALValue::array(0.00149202, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[1][3] = 5.5;
    keys[1][3] = AL::ALValue::array(-0.00157595, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[1][4] = 8.5;
    keys[1][4] = AL::ALValue::array(0.00149202, AL::ALValue::array(3, -1, -0.00102266), AL::ALValue::array(3, 0.5, 0.000511329));
    times[1][5] = 10;
    keys[1][5] = AL::ALValue::array(0.00302602, AL::ALValue::array(3, -0.5, -0.00127832), AL::ALValue::array(3, 0.5, 0.00127832));
    times[1][6] = 11.5;
    keys[1][6] = AL::ALValue::array(0.00916195, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[1][7] = 13;
    keys[1][7] = AL::ALValue::array(0.00455999, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[1][8] = 14.5;
    keys[1][8] = AL::ALValue::array(0.00762796, AL::ALValue::array(3, -0.5, -0.00306798), AL::ALValue::array(3, 0.5, 0.00306798));
    times[1][9] = 16;
    keys[1][9] = AL::ALValue::array(0.022968, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[1][10] = 17.5;
    keys[1][10] = AL::ALValue::array(0.0122299, AL::ALValue::array(3, -0.5, 0.00230101), AL::ALValue::array(3, 0.5, -0.00230101));
    times[1][11] = 19;
    keys[1][11] = AL::ALValue::array(0.00916195, AL::ALValue::array(3, -0.5, 0.000766994), AL::ALValue::array(3, 0.833333, -0.00127832));
    times[1][12] = 21.5;
    keys[1][12] = AL::ALValue::array(0.00609397, AL::ALValue::array(3, -0.833333, 0), AL::ALValue::array(3, 0.5, 0));
    times[1][13] = 23;
    keys[1][13] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(14);
    keys[2].arraySetSize(14);

    times[2][0] = 0.5;
    keys[2][0] = AL::ALValue::array(-0.414222, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[2][1] = 2.5;
    keys[2][1] = AL::ALValue::array(-0.385075, AL::ALValue::array(3, -0.666667, -0.00730478), AL::ALValue::array(3, 0.5, 0.00547859));
    times[2][2] = 4;
    keys[2][2] = AL::ALValue::array(-0.375872, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[2][3] = 5.5;
    keys[2][3] = AL::ALValue::array(-0.377407, AL::ALValue::array(3, -0.5, 0.00153415), AL::ALValue::array(3, 1, -0.00306829));
    times[2][4] = 8.5;
    keys[2][4] = AL::ALValue::array(-0.441834, AL::ALValue::array(3, -1, 0.0340886), AL::ALValue::array(3, 0.5, -0.0170443));
    times[2][5] = 10;
    keys[2][5] = AL::ALValue::array(-0.530805, AL::ALValue::array(3, -0.5, 0.00306826), AL::ALValue::array(3, 0.5, -0.00306826));
    times[2][6] = 11.5;
    keys[2][6] = AL::ALValue::array(-0.533873, AL::ALValue::array(3, -0.5, 0.00306826), AL::ALValue::array(3, 0.5, -0.00306826));
    times[2][7] = 13;
    keys[2][7] = AL::ALValue::array(-0.895898, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[2][8] = 14.5;
    keys[2][8] = AL::ALValue::array(-0.199461, AL::ALValue::array(3, -0.5, -0.152889), AL::ALValue::array(3, 0.5, 0.152889));
    times[2][9] = 16;
    keys[2][9] = AL::ALValue::array(0.021434, AL::ALValue::array(3, -0.5, -0.0490879), AL::ALValue::array(3, 0.5, 0.0490879));
    times[2][10] = 17.5;
    keys[2][10] = AL::ALValue::array(0.095066, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[2][11] = 19;
    keys[2][11] = AL::ALValue::array(-0.0153821, AL::ALValue::array(3, -0.5, 0.0475541), AL::ALValue::array(3, 0.833333, -0.0792568));
    times[2][12] = 21.5;
    keys[2][12] = AL::ALValue::array(-0.285367, AL::ALValue::array(3, -0.833333, 0.0830918), AL::ALValue::array(3, 0.5, -0.0498551));
    times[2][13] = 23;
    keys[2][13] = AL::ALValue::array(-0.414222, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(14);
    keys[3].arraySetSize(14);

    times[3][0] = 0.5;
    keys[3][0] = AL::ALValue::array(0.11816, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[3][1] = 2.5;
    keys[3][1] = AL::ALValue::array(0.242414, AL::ALValue::array(3, -0.666667, -0.0122708), AL::ALValue::array(3, 0.5, 0.00920312));
    times[3][2] = 4;
    keys[3][2] = AL::ALValue::array(0.251617, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[3][3] = 5.5;
    keys[3][3] = AL::ALValue::array(0.248551, AL::ALValue::array(3, -0.5, 0.00306655), AL::ALValue::array(3, 1, -0.00613309));
    times[3][4] = 8.5;
    keys[3][4] = AL::ALValue::array(0.11049, AL::ALValue::array(3, -1, 0.0620419), AL::ALValue::array(3, 0.5, -0.031021));
    times[3][5] = 10;
    keys[3][5] = AL::ALValue::array(-0.030638, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[3][6] = 11.5;
    keys[3][6] = AL::ALValue::array(-0.0275701, AL::ALValue::array(3, -0.5, -0.00306794), AL::ALValue::array(3, 0.5, 0.00306794));
    times[3][7] = 13;
    keys[3][7] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.5, -0.00306798), AL::ALValue::array(3, 0.5, 0.00306798));
    times[3][8] = 14.5;
    keys[3][8] = AL::ALValue::array(4.19617e-05, AL::ALValue::array(3, -0.5, -0.00306798), AL::ALValue::array(3, 0.5, 0.00306798));
    times[3][9] = 16;
    keys[3][9] = AL::ALValue::array(0.0291878, AL::ALValue::array(3, -0.5, -0.00639167), AL::ALValue::array(3, 0.5, 0.00639167));
    times[3][10] = 17.5;
    keys[3][10] = AL::ALValue::array(0.038392, AL::ALValue::array(3, -0.5, -0.00306802), AL::ALValue::array(3, 0.5, 0.00306802));
    times[3][11] = 19;
    keys[3][11] = AL::ALValue::array(0.047596, AL::ALValue::array(3, -0.5, -0.00632776), AL::ALValue::array(3, 0.833333, 0.0105463));
    times[3][12] = 21.5;
    keys[3][12] = AL::ALValue::array(0.0890141, AL::ALValue::array(3, -0.833333, -0.0147008), AL::ALValue::array(3, 0.5, 0.0088205));
    times[3][13] = 23;
    keys[3][13] = AL::ALValue::array(0.11816, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(14);
    keys[4].arraySetSize(14);

    times[4][0] = 0.5;
    keys[4][0] = AL::ALValue::array(-0.291418, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[4][1] = 2.5;
    keys[4][1] = AL::ALValue::array(-0.285283, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[4][2] = 4;
    keys[4][2] = AL::ALValue::array(-0.292952, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[4][3] = 5.5;
    keys[4][3] = AL::ALValue::array(-0.286817, AL::ALValue::array(3, -0.5, -0.00221579), AL::ALValue::array(3, 1, 0.00443159));
    times[4][4] = 8.5;
    keys[4][4] = AL::ALValue::array(-0.27301, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[4][5] = 10;
    keys[4][5] = AL::ALValue::array(-0.31903, AL::ALValue::array(3, -0.5, 0.0112495), AL::ALValue::array(3, 0.5, -0.0112495));
    times[4][6] = 11.5;
    keys[4][6] = AL::ALValue::array(-0.340507, AL::ALValue::array(3, -0.5, 0.00306656), AL::ALValue::array(3, 0.5, -0.00306656));
    times[4][7] = 13;
    keys[4][7] = AL::ALValue::array(-0.343573, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[4][8] = 14.5;
    keys[4][8] = AL::ALValue::array(-0.305223, AL::ALValue::array(3, -0.5, -0.00766901), AL::ALValue::array(3, 0.5, 0.00766901));
    times[4][9] = 16;
    keys[4][9] = AL::ALValue::array(-0.297554, AL::ALValue::array(3, -0.5, -0.00766901), AL::ALValue::array(3, 0.5, 0.00766901));
    times[4][10] = 17.5;
    keys[4][10] = AL::ALValue::array(-0.170232, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[4][11] = 19;
    keys[4][11] = AL::ALValue::array(-0.197844, AL::ALValue::array(3, -0.5, 0.0101627), AL::ALValue::array(3, 0.833333, -0.0169378));
    times[4][12] = 21.5;
    keys[4][12] = AL::ALValue::array(-0.251533, AL::ALValue::array(3, -0.833333, 0.0194945), AL::ALValue::array(3, 0.5, -0.0116967));
    times[4][13] = 23;
    keys[4][13] = AL::ALValue::array(-0.291418, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(14);
    keys[5].arraySetSize(14);

    times[5][0] = 0.5;
    keys[5][0] = AL::ALValue::array(-1.25025, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[5][1] = 2.5;
    keys[5][1] = AL::ALValue::array(-1.24412, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[5][2] = 4;
    keys[5][2] = AL::ALValue::array(-1.25179, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[5][3] = 5.5;
    keys[5][3] = AL::ALValue::array(-1.24718, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[5][4] = 8.5;
    keys[5][4] = AL::ALValue::array(-1.24872, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[5][5] = 10;
    keys[5][5] = AL::ALValue::array(-1.24258, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[5][6] = 11.5;
    keys[5][6] = AL::ALValue::array(-1.24718, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[5][7] = 13;
    keys[5][7] = AL::ALValue::array(-1.22878, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[5][8] = 14.5;
    keys[5][8] = AL::ALValue::array(-1.23491, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[5][9] = 16;
    keys[5][9] = AL::ALValue::array(-1.23491, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[5][10] = 17.5;
    keys[5][10] = AL::ALValue::array(-1.28707, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[5][11] = 19;
    keys[5][11] = AL::ALValue::array(-1.26866, AL::ALValue::array(3, -0.5, -0.000920149), AL::ALValue::array(3, 0.833333, 0.00153358));
    times[5][12] = 21.5;
    keys[5][12] = AL::ALValue::array(-1.26713, AL::ALValue::array(3, -0.833333, -0.00153358), AL::ALValue::array(3, 0.5, 0.000920149));
    times[5][13] = 23;
    keys[5][13] = AL::ALValue::array(-1.25025, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(14);
    keys[6].arraySetSize(14);

    times[6][0] = 0.5;
    keys[6][0] = AL::ALValue::array(0.2832, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[6][1] = 2.5;
    keys[6][1] = AL::ALValue::array(0.2856, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[6][2] = 4;
    keys[6][2] = AL::ALValue::array(0.2844, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[6][3] = 5.5;
    keys[6][3] = AL::ALValue::array(0.3024, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[6][4] = 8.5;
    keys[6][4] = AL::ALValue::array(0.286, AL::ALValue::array(3, -1, 0.00444444), AL::ALValue::array(3, 0.5, -0.00222222));
    times[6][5] = 10;
    keys[6][5] = AL::ALValue::array(0.2824, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[6][6] = 11.5;
    keys[6][6] = AL::ALValue::array(0.2908, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[6][7] = 13;
    keys[6][7] = AL::ALValue::array(0.2888, AL::ALValue::array(3, -0.5, 0.000866666), AL::ALValue::array(3, 0.5, -0.000866666));
    times[6][8] = 14.5;
    keys[6][8] = AL::ALValue::array(0.2856, AL::ALValue::array(3, -0.5, 0.00106666), AL::ALValue::array(3, 0.5, -0.00106666));
    times[6][9] = 16;
    keys[6][9] = AL::ALValue::array(0.2824, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[6][10] = 17.5;
    keys[6][10] = AL::ALValue::array(0.284, AL::ALValue::array(3, -0.5, -0.00126667), AL::ALValue::array(3, 0.5, 0.00126667));
    times[6][11] = 19;
    keys[6][11] = AL::ALValue::array(0.29, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.833333, 0));
    times[6][12] = 21.5;
    keys[6][12] = AL::ALValue::array(0.2872, AL::ALValue::array(3, -0.833333, 0.00141667), AL::ALValue::array(3, 0.5, -0.000850003));
    times[6][13] = 23;
    keys[6][13] = AL::ALValue::array(0.2832, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(14);
    keys[7].arraySetSize(14);

    times[7][0] = 0.5;
    keys[7][0] = AL::ALValue::array(0.326783, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[7][1] = 2.5;
    keys[7][1] = AL::ALValue::array(0.191792, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[7][2] = 4;
    keys[7][2] = AL::ALValue::array(0.194861, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[7][3] = 5.5;
    keys[7][3] = AL::ALValue::array(0.193327, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[7][4] = 8.5;
    keys[7][4] = AL::ALValue::array(0.214803, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[7][5] = 10;
    keys[7][5] = AL::ALValue::array(0.150374, AL::ALValue::array(3, -0.5, 0.00306794), AL::ALValue::array(3, 0.5, -0.00306794));
    times[7][6] = 11.5;
    keys[7][6] = AL::ALValue::array(0.147306, AL::ALValue::array(3, -0.5, 0.00306794), AL::ALValue::array(3, 0.5, -0.00306794));
    times[7][7] = 13;
    keys[7][7] = AL::ALValue::array(-0.266875, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[7][8] = 14.5;
    keys[7][8] = AL::ALValue::array(0.154976, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[7][9] = 16;
    keys[7][9] = AL::ALValue::array(-0.118076, AL::ALValue::array(3, -0.5, 0.0588032), AL::ALValue::array(3, 0.5, -0.0588032));
    times[7][10] = 17.5;
    keys[7][10] = AL::ALValue::array(-0.197844, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[7][11] = 19;
    keys[7][11] = AL::ALValue::array(-0.182504, AL::ALValue::array(3, -0.5, -0.0153396), AL::ALValue::array(3, 0.833333, 0.025566));
    times[7][12] = 21.5;
    keys[7][12] = AL::ALValue::array(0.211735, AL::ALValue::array(3, -0.833333, -0.106102), AL::ALValue::array(3, 0.5, 0.0636609));
    times[7][13] = 23;
    keys[7][13] = AL::ALValue::array(0.326783, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(14);
    keys[8].arraySetSize(14);

    times[8][0] = 0.5;
    keys[8][0] = AL::ALValue::array(-0.11961, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[8][1] = 2.5;
    keys[8][1] = AL::ALValue::array(-0.13495, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[8][2] = 4;
    keys[8][2] = AL::ALValue::array(-0.131882, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[8][3] = 5.5;
    keys[8][3] = AL::ALValue::array(-0.131882, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[8][4] = 8.5;
    keys[8][4] = AL::ALValue::array(-0.154892, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[8][5] = 10;
    keys[8][5] = AL::ALValue::array(-0.138018, AL::ALValue::array(3, -0.5, -0.00153396), AL::ALValue::array(3, 0.5, 0.00153396));
    times[8][6] = 11.5;
    keys[8][6] = AL::ALValue::array(-0.136484, AL::ALValue::array(3, -0.5, -0.00153396), AL::ALValue::array(3, 0.5, 0.00153396));
    times[8][7] = 13;
    keys[8][7] = AL::ALValue::array(-0.076658, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[8][8] = 14.5;
    keys[8][8] = AL::ALValue::array(-0.305223, AL::ALValue::array(3, -0.5, 0.0457643), AL::ALValue::array(3, 0.5, -0.0457643));
    times[8][9] = 16;
    keys[8][9] = AL::ALValue::array(-0.351244, AL::ALValue::array(3, -0.5, 0.0120166), AL::ALValue::array(3, 0.5, -0.0120166));
    times[8][10] = 17.5;
    keys[8][10] = AL::ALValue::array(-0.377323, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[8][11] = 19;
    keys[8][11] = AL::ALValue::array(-0.338972, AL::ALValue::array(3, -0.5, -0.0232018), AL::ALValue::array(3, 0.833333, 0.0386696));
    times[8][12] = 21.5;
    keys[8][12] = AL::ALValue::array(-0.191709, AL::ALValue::array(3, -0.833333, -0.0457004), AL::ALValue::array(3, 0.5, 0.0274203));
    times[8][13] = 23;
    keys[8][13] = AL::ALValue::array(-0.11961, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(14);
    keys[9].arraySetSize(14);

    times[9][0] = 0.5;
    keys[9][0] = AL::ALValue::array(-0.0137641, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[9][1] = 2.5;
    keys[9][1] = AL::ALValue::array(-0.0889301, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[9][2] = 4;
    keys[9][2] = AL::ALValue::array(-0.0889301, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[9][3] = 5.5;
    keys[9][3] = AL::ALValue::array(-0.0966001, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[9][4] = 8.5;
    keys[9][4] = AL::ALValue::array(-0.0904641, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[9][5] = 10;
    keys[9][5] = AL::ALValue::array(-0.251533, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[9][6] = 11.5;
    keys[9][6] = AL::ALValue::array(-0.248467, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[9][7] = 13;
    keys[9][7] = AL::ALValue::array(-0.42641, AL::ALValue::array(3, -0.5, 0.0764443), AL::ALValue::array(3, 0.5, -0.0764443));
    times[9][8] = 14.5;
    keys[9][8] = AL::ALValue::array(-0.707132, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[9][9] = 16;
    keys[9][9] = AL::ALValue::array(-0.432547, AL::ALValue::array(3, -0.5, -0.0958751), AL::ALValue::array(3, 0.5, 0.0958751));
    times[9][10] = 17.5;
    keys[9][10] = AL::ALValue::array(-0.131882, AL::ALValue::array(3, -0.5, -0.00460197), AL::ALValue::array(3, 0.5, 0.00460197));
    times[9][11] = 19;
    keys[9][11] = AL::ALValue::array(-0.12728, AL::ALValue::array(3, -0.5, -0.00460197), AL::ALValue::array(3, 0.833333, 0.00766995));
    times[9][12] = 21.5;
    keys[9][12] = AL::ALValue::array(0.01845, AL::ALValue::array(3, -0.833333, 0), AL::ALValue::array(3, 0.5, 0));
    times[9][13] = 23;
    keys[9][13] = AL::ALValue::array(-0.0137641, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(14);
    keys[10].arraySetSize(14);

    times[10][0] = 0.5;
    keys[10][0] = AL::ALValue::array(0.144154, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[10][1] = 2.5;
    keys[10][1] = AL::ALValue::array(0.193243, AL::ALValue::array(3, -0.666667, -0.0102253), AL::ALValue::array(3, 0.5, 0.00766899));
    times[10][2] = 4;
    keys[10][2] = AL::ALValue::array(0.200912, AL::ALValue::array(3, -0.5, -0.00204524), AL::ALValue::array(3, 0.5, 0.00204524));
    times[10][3] = 5.5;
    keys[10][3] = AL::ALValue::array(0.205514, AL::ALValue::array(3, -0.5, -0.00289763), AL::ALValue::array(3, 1, 0.00579527));
    times[10][4] = 8.5;
    keys[10][4] = AL::ALValue::array(0.226991, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[10][5] = 10;
    keys[10][5] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[10][6] = 11.5;
    keys[10][6] = AL::ALValue::array(0.216252, AL::ALValue::array(3, -0.5, -0.00153415), AL::ALValue::array(3, 0.5, 0.00153415));
    times[10][7] = 13;
    keys[10][7] = AL::ALValue::array(1.45112, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[10][8] = 14.5;
    keys[10][8] = AL::ALValue::array(0.171766, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[10][9] = 16;
    keys[10][9] = AL::ALValue::array(0.205514, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[10][10] = 17.5;
    keys[10][10] = AL::ALValue::array(0.194775, AL::ALValue::array(3, -0.5, 0.010739), AL::ALValue::array(3, 0.5, -0.010739));
    times[10][11] = 19;
    keys[10][11] = AL::ALValue::array(0.105804, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.833333, 0));
    times[10][12] = 21.5;
    keys[10][12] = AL::ALValue::array(0.151824, AL::ALValue::array(3, -0.833333, 0), AL::ALValue::array(3, 0.5, 0));
    times[10][13] = 23;
    keys[10][13] = AL::ALValue::array(0.144154, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(14);
    keys[11].arraySetSize(14);

    times[11][0] = 0.5;
    keys[11][0] = AL::ALValue::array(1.89291, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[11][1] = 2.5;
    keys[11][1] = AL::ALValue::array(1.86223, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[11][2] = 4;
    keys[11][2] = AL::ALValue::array(1.8745, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[11][3] = 5.5;
    keys[11][3] = AL::ALValue::array(1.8653, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[11][4] = 8.5;
    keys[11][4] = AL::ALValue::array(1.87604, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[11][5] = 10;
    keys[11][5] = AL::ALValue::array(1.37442, AL::ALValue::array(3, -0.5, 0.0398842), AL::ALValue::array(3, 0.5, -0.0398842));
    times[11][6] = 11.5;
    keys[11][6] = AL::ALValue::array(1.33454, AL::ALValue::array(3, -0.5, 0.0201978), AL::ALValue::array(3, 0.5, -0.0201978));
    times[11][7] = 13;
    keys[11][7] = AL::ALValue::array(1.25324, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[11][8] = 14.5;
    keys[11][8] = AL::ALValue::array(1.30693, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[11][9] = 16;
    keys[11][9] = AL::ALValue::array(1.30693, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[11][10] = 17.5;
    keys[11][10] = AL::ALValue::array(1.56771, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[11][11] = 19;
    keys[11][11] = AL::ALValue::array(1.53242, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.833333, 0));
    times[11][12] = 21.5;
    keys[11][12] = AL::ALValue::array(1.77633, AL::ALValue::array(3, -0.833333, -0.0751021), AL::ALValue::array(3, 0.5, 0.0450613));
    times[11][13] = 23;
    keys[11][13] = AL::ALValue::array(1.89291, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(14);
    keys[12].arraySetSize(14);

    times[12][0] = 0.5;
    keys[12][0] = AL::ALValue::array(0.147222, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[12][1] = 2.5;
    keys[12][1] = AL::ALValue::array(0.179436, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[12][2] = 4;
    keys[12][2] = AL::ALValue::array(0.125746, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[12][3] = 5.5;
    keys[12][3] = AL::ALValue::array(0.190175, AL::ALValue::array(3, -0.5, -0.00766985), AL::ALValue::array(3, 1, 0.0153397));
    times[12][4] = 8.5;
    keys[12][4] = AL::ALValue::array(0.205514, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[12][5] = 10;
    keys[12][5] = AL::ALValue::array(0.148756, AL::ALValue::array(3, -0.5, 0.0140617), AL::ALValue::array(3, 0.5, -0.0140617));
    times[12][6] = 11.5;
    keys[12][6] = AL::ALValue::array(0.121144, AL::ALValue::array(3, -0.5, 0.0132946), AL::ALValue::array(3, 0.5, -0.0132946));
    times[12][7] = 13;
    keys[12][7] = AL::ALValue::array(0.0689882, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[12][8] = 14.5;
    keys[12][8] = AL::ALValue::array(0.12728, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[12][9] = 16;
    keys[12][9] = AL::ALValue::array(-0.14117, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[12][10] = 17.5;
    keys[12][10] = AL::ALValue::array(0.70253, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[12][11] = 19;
    keys[12][11] = AL::ALValue::array(0.615092, AL::ALValue::array(3, -0.5, 0.0429519), AL::ALValue::array(3, 0.833333, -0.0715865));
    times[12][12] = 21.5;
    keys[12][12] = AL::ALValue::array(0.358915, AL::ALValue::array(3, -0.833333, 0.0974729), AL::ALValue::array(3, 0.5, -0.0584838));
    times[12][13] = 23;
    keys[12][13] = AL::ALValue::array(0.147222, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(14);
    keys[13].arraySetSize(14);

    times[13][0] = 0.5;
    keys[13][0] = AL::ALValue::array(-0.943452, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[13][1] = 2.5;
    keys[13][1] = AL::ALValue::array(-0.905102, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[13][2] = 4;
    keys[13][2] = AL::ALValue::array(-0.92351, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[13][3] = 5.5;
    keys[13][3] = AL::ALValue::array(-0.894364, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[13][4] = 8.5;
    keys[13][4] = AL::ALValue::array(-0.906636, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[13][5] = 10;
    keys[13][5] = AL::ALValue::array(-0.906636, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[13][6] = 11.5;
    keys[13][6] = AL::ALValue::array(-0.911238, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[13][7] = 13;
    keys[13][7] = AL::ALValue::array(-0.883625, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[13][8] = 14.5;
    keys[13][8] = AL::ALValue::array(-0.902033, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[13][9] = 16;
    keys[13][9] = AL::ALValue::array(-0.895898, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[13][10] = 17.5;
    keys[13][10] = AL::ALValue::array(-1.01862, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[13][11] = 19;
    keys[13][11] = AL::ALValue::array(-0.991006, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.833333, 0));
    times[13][12] = 21.5;
    keys[13][12] = AL::ALValue::array(-0.991007, AL::ALValue::array(3, -0.833333, 0), AL::ALValue::array(3, 0.5, 0));
    times[13][13] = 23;
    keys[13][13] = AL::ALValue::array(-0.943452, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(14);
    keys[14].arraySetSize(14);

    times[14][0] = 0.5;
    keys[14][0] = AL::ALValue::array(-0.454021, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[14][1] = 2.5;
    keys[14][1] = AL::ALValue::array(-0.28068, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[14][2] = 4;
    keys[14][2] = AL::ALValue::array(-1.17807, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[14][3] = 5.5;
    keys[14][3] = AL::ALValue::array(-0.249999, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[14][4] = 8.5;
    keys[14][4] = AL::ALValue::array(-0.256136, AL::ALValue::array(3, -1, 0.00613657), AL::ALValue::array(3, 0.5, -0.00306829));
    times[14][5] = 10;
    keys[14][5] = AL::ALValue::array(-0.299088, AL::ALValue::array(3, -0.5, 0.0429525), AL::ALValue::array(3, 0.5, -0.0429525));
    times[14][6] = 11.5;
    keys[14][6] = AL::ALValue::array(-0.592082, AL::ALValue::array(3, -0.5, 0.0631495), AL::ALValue::array(3, 0.5, -0.0631495));
    times[14][7] = 13;
    keys[14][7] = AL::ALValue::array(-0.677985, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[14][8] = 14.5;
    keys[14][8] = AL::ALValue::array(-0.427944, AL::ALValue::array(3, -0.5, -0.0695412), AL::ALValue::array(3, 0.5, 0.0695412));
    times[14][9] = 16;
    keys[14][9] = AL::ALValue::array(-0.260738, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[14][10] = 17.5;
    keys[14][10] = AL::ALValue::array(-0.338973, AL::ALValue::array(3, -0.5, 0.0357933), AL::ALValue::array(3, 0.5, -0.0357933));
    times[14][11] = 19;
    keys[14][11] = AL::ALValue::array(-0.475498, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.833333, 0));
    times[14][12] = 21.5;
    keys[14][12] = AL::ALValue::array(-0.418739, AL::ALValue::array(3, -0.833333, 0), AL::ALValue::array(3, 0.5, 0));
    times[14][13] = 23;
    keys[14][13] = AL::ALValue::array(-0.454021, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(14);
    keys[15].arraySetSize(14);

    times[15][0] = 0.5;
    keys[15][0] = AL::ALValue::array(0.07214, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[15][1] = 2.5;
    keys[15][1] = AL::ALValue::array(0.213269, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[15][2] = 4;
    keys[15][2] = AL::ALValue::array(-0.0536479, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[15][3] = 5.5;
    keys[15][3] = AL::ALValue::array(0.142704, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[15][4] = 8.5;
    keys[15][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -1, 0.0668142), AL::ALValue::array(3, 0.5, -0.0334071));
    times[15][5] = 10;
    keys[15][5] = AL::ALValue::array(-0.15796, AL::ALValue::array(3, -0.5, 0.0322139), AL::ALValue::array(3, 0.5, -0.0322139));
    times[15][6] = 11.5;
    keys[15][6] = AL::ALValue::array(-0.196309, AL::ALValue::array(3, -0.5, 0.0183971), AL::ALValue::array(3, 0.5, -0.0183971));
    times[15][7] = 13;
    keys[15][7] = AL::ALValue::array(-0.268343, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[15][8] = 14.5;
    keys[15][8] = AL::ALValue::array(-0.151824, AL::ALValue::array(3, -0.5, -0.0316918), AL::ALValue::array(3, 0.5, 0.0316918));
    times[15][9] = 16;
    keys[15][9] = AL::ALValue::array(-0.078192, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[15][10] = 17.5;
    keys[15][10] = AL::ALValue::array(-0.093532, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[15][11] = 19;
    keys[15][11] = AL::ALValue::array(-0.0674541, AL::ALValue::array(3, -0.5, -0.0132307), AL::ALValue::array(3, 0.833333, 0.0220512));
    times[15][12] = 21.5;
    keys[15][12] = AL::ALValue::array(0.0123138, AL::ALValue::array(3, -0.833333, -0.0290821), AL::ALValue::array(3, 0.5, 0.0174493));
    times[15][13] = 23;
    keys[15][13] = AL::ALValue::array(0.07214, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(14);
    keys[16].arraySetSize(14);

    times[16][0] = 0.5;
    keys[16][0] = AL::ALValue::array(0.435699, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[16][1] = 2.5;
    keys[16][1] = AL::ALValue::array(0.443368, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[16][2] = 4;
    keys[16][2] = AL::ALValue::array(0.431096, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[16][3] = 5.5;
    keys[16][3] = AL::ALValue::array(0.44797, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[16][4] = 8.5;
    keys[16][4] = AL::ALValue::array(0.44797, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[16][5] = 10;
    keys[16][5] = AL::ALValue::array(0.44797, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[16][6] = 11.5;
    keys[16][6] = AL::ALValue::array(0.441834, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[16][7] = 13;
    keys[16][7] = AL::ALValue::array(0.444902, AL::ALValue::array(3, -0.5, -0.000767068), AL::ALValue::array(3, 0.5, 0.000767068));
    times[16][8] = 14.5;
    keys[16][8] = AL::ALValue::array(0.446436, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[16][9] = 16;
    keys[16][9] = AL::ALValue::array(0.446436, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[16][10] = 17.5;
    keys[16][10] = AL::ALValue::array(0.443368, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[16][11] = 19;
    keys[16][11] = AL::ALValue::array(0.452572, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.833333, 0));
    times[16][12] = 21.5;
    keys[16][12] = AL::ALValue::array(0.440299, AL::ALValue::array(3, -0.833333, 0.00351523), AL::ALValue::array(3, 0.5, -0.00210914));
    times[16][13] = 23;
    keys[16][13] = AL::ALValue::array(0.435699, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(14);
    keys[17].arraySetSize(14);

    times[17][0] = 0.5;
    keys[17][0] = AL::ALValue::array(1.26858, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[17][1] = 2.5;
    keys[17][1] = AL::ALValue::array(1.27164, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[17][2] = 4;
    keys[17][2] = AL::ALValue::array(1.27164, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[17][3] = 5.5;
    keys[17][3] = AL::ALValue::array(1.26551, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[17][4] = 8.5;
    keys[17][4] = AL::ALValue::array(1.27164, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[17][5] = 10;
    keys[17][5] = AL::ALValue::array(1.26704, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[17][6] = 11.5;
    keys[17][6] = AL::ALValue::array(1.27931, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[17][7] = 13;
    keys[17][7] = AL::ALValue::array(1.27164, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[17][8] = 14.5;
    keys[17][8] = AL::ALValue::array(1.27164, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[17][9] = 16;
    keys[17][9] = AL::ALValue::array(1.27625, AL::ALValue::array(3, -0.5, -0.00153414), AL::ALValue::array(3, 0.5, 0.00153414));
    times[17][10] = 17.5;
    keys[17][10] = AL::ALValue::array(1.28085, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[17][11] = 19;
    keys[17][11] = AL::ALValue::array(1.27778, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.833333, 0));
    times[17][12] = 21.5;
    keys[17][12] = AL::ALValue::array(1.27778, AL::ALValue::array(3, -0.833333, 0), AL::ALValue::array(3, 0.5, 0));
    times[17][13] = 23;
    keys[17][13] = AL::ALValue::array(1.26858, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(14);
    keys[18].arraySetSize(14);

    times[18][0] = 0.5;
    keys[18][0] = AL::ALValue::array(0.3012, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[18][1] = 2.5;
    keys[18][1] = AL::ALValue::array(0.2912, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[18][2] = 4;
    keys[18][2] = AL::ALValue::array(0.2972, AL::ALValue::array(3, -0.5, -0.0036), AL::ALValue::array(3, 0.5, 0.0036));
    times[18][3] = 5.5;
    keys[18][3] = AL::ALValue::array(0.3128, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[18][4] = 8.5;
    keys[18][4] = AL::ALValue::array(0.2972, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[18][5] = 10;
    keys[18][5] = AL::ALValue::array(0.2992, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[18][6] = 11.5;
    keys[18][6] = AL::ALValue::array(0.2984, AL::ALValue::array(3, -0.5, 0.000799984), AL::ALValue::array(3, 0.5, -0.000799984));
    times[18][7] = 13;
    keys[18][7] = AL::ALValue::array(0.2944, AL::ALValue::array(3, -0.5, 0.0016), AL::ALValue::array(3, 0.5, -0.0016));
    times[18][8] = 14.5;
    keys[18][8] = AL::ALValue::array(0.2888, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[18][9] = 16;
    keys[18][9] = AL::ALValue::array(0.2896, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[18][10] = 17.5;
    keys[18][10] = AL::ALValue::array(0.2888, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[18][11] = 19;
    keys[18][11] = AL::ALValue::array(0.2916, AL::ALValue::array(3, -0.5, -0.0012), AL::ALValue::array(3, 0.833333, 0.002));
    times[18][12] = 21.5;
    keys[18][12] = AL::ALValue::array(0.2984, AL::ALValue::array(3, -0.833333, -0.002), AL::ALValue::array(3, 0.5, 0.0012));
    times[18][13] = 23;
    keys[18][13] = AL::ALValue::array(0.3012, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(14);
    keys[19].arraySetSize(14);

    times[19][0] = 0.5;
    keys[19][0] = AL::ALValue::array(0.269941, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[19][1] = 2.5;
    keys[19][1] = AL::ALValue::array(0.0260359, AL::ALValue::array(3, -0.666667, 0.102851), AL::ALValue::array(3, 0.5, -0.0771381));
    times[19][2] = 4;
    keys[19][2] = AL::ALValue::array(-0.270025, AL::ALValue::array(3, -0.5, 0.121442), AL::ALValue::array(3, 0.5, -0.121442));
    times[19][3] = 5.5;
    keys[19][3] = AL::ALValue::array(-0.702614, AL::ALValue::array(3, -0.5, 0.0726094), AL::ALValue::array(3, 1, -0.145219));
    times[19][4] = 8.5;
    keys[19][4] = AL::ALValue::array(-0.92351, AL::ALValue::array(3, -1, 0.140446), AL::ALValue::array(3, 0.5, -0.0702231));
    times[19][5] = 10;
    keys[19][5] = AL::ALValue::array(-1.33462, AL::ALValue::array(3, -0.5, 0.0851372), AL::ALValue::array(3, 0.5, -0.0851372));
    times[19][6] = 11.5;
    keys[19][6] = AL::ALValue::array(-1.43433, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[19][7] = 13;
    keys[19][7] = AL::ALValue::array(-0.93118, AL::ALValue::array(3, -0.5, -0.118118), AL::ALValue::array(3, 0.5, 0.118118));
    times[19][8] = 14.5;
    keys[19][8] = AL::ALValue::array(-0.725624, AL::ALValue::array(3, -0.5, -0.0153397), AL::ALValue::array(3, 0.5, 0.0153397));
    times[19][9] = 16;
    keys[19][9] = AL::ALValue::array(-0.710284, AL::ALValue::array(3, -0.5, -0.0153397), AL::ALValue::array(3, 0.5, 0.0153397));
    times[19][10] = 17.5;
    keys[19][10] = AL::ALValue::array(-0.454105, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[19][11] = 19;
    keys[19][11] = AL::ALValue::array(-0.613642, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.833333, 0));
    times[19][12] = 21.5;
    keys[19][12] = AL::ALValue::array(0.059784, AL::ALValue::array(3, -0.833333, -0.18408), AL::ALValue::array(3, 0.5, 0.110448));
    times[19][13] = 23;
    keys[19][13] = AL::ALValue::array(0.269941, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(14);
    keys[20].arraySetSize(14);

    times[20][0] = 0.5;
    keys[20][0] = AL::ALValue::array(-0.0705221, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[20][1] = 2.5;
    keys[20][1] = AL::ALValue::array(-0.167164, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[20][2] = 4;
    keys[20][2] = AL::ALValue::array(-0.0705221, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[20][3] = 5.5;
    keys[20][3] = AL::ALValue::array(-0.113474, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[20][4] = 8.5;
    keys[20][4] = AL::ALValue::array(-0.0367741, AL::ALValue::array(3, -1, -0.0248849), AL::ALValue::array(3, 0.5, 0.0124425));
    times[20][5] = 10;
    keys[20][5] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[20][6] = 11.5;
    keys[20][6] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[20][7] = 13;
    keys[20][7] = AL::ALValue::array(0.0261199, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[20][8] = 14.5;
    keys[20][8] = AL::ALValue::array(-0.0843279, AL::ALValue::array(3, -0.5, 0.0373272), AL::ALValue::array(3, 0.5, -0.0373272));
    times[20][9] = 16;
    keys[20][9] = AL::ALValue::array(-0.197844, AL::ALValue::array(3, -0.5, 0.0391171), AL::ALValue::array(3, 0.5, -0.0391171));
    times[20][10] = 17.5;
    keys[20][10] = AL::ALValue::array(-0.31903, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[20][11] = 19;
    keys[20][11] = AL::ALValue::array(-0.266874, AL::ALValue::array(3, -0.5, -0.0178329), AL::ALValue::array(3, 0.833333, 0.0297215));
    times[20][12] = 21.5;
    keys[20][12] = AL::ALValue::array(-0.176367, AL::ALValue::array(3, -0.833333, -0.0409067), AL::ALValue::array(3, 0.5, 0.024544));
    times[20][13] = 23;
    keys[20][13] = AL::ALValue::array(-0.0705221, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(14);
    keys[21].arraySetSize(14);

    times[21][0] = 0.5;
    keys[21][0] = AL::ALValue::array(-0.0137641, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[21][1] = 2.5;
    keys[21][1] = AL::ALValue::array(-0.0889301, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[21][2] = 4;
    keys[21][2] = AL::ALValue::array(-0.0889301, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[21][3] = 5.5;
    keys[21][3] = AL::ALValue::array(-0.0966001, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[21][4] = 8.5;
    keys[21][4] = AL::ALValue::array(-0.0904641, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[21][5] = 10;
    keys[21][5] = AL::ALValue::array(-0.251533, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[21][6] = 11.5;
    keys[21][6] = AL::ALValue::array(-0.248467, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[21][7] = 13;
    keys[21][7] = AL::ALValue::array(-0.42641, AL::ALValue::array(3, -0.5, 0.0764443), AL::ALValue::array(3, 0.5, -0.0764443));
    times[21][8] = 14.5;
    keys[21][8] = AL::ALValue::array(-0.707132, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[21][9] = 16;
    keys[21][9] = AL::ALValue::array(-0.432547, AL::ALValue::array(3, -0.5, -0.0958751), AL::ALValue::array(3, 0.5, 0.0958751));
    times[21][10] = 17.5;
    keys[21][10] = AL::ALValue::array(-0.131882, AL::ALValue::array(3, -0.5, -0.00460197), AL::ALValue::array(3, 0.5, 0.00460197));
    times[21][11] = 19;
    keys[21][11] = AL::ALValue::array(-0.12728, AL::ALValue::array(3, -0.5, -0.00460197), AL::ALValue::array(3, 0.833333, 0.00766995));
    times[21][12] = 21.5;
    keys[21][12] = AL::ALValue::array(0.01845, AL::ALValue::array(3, -0.833333, 0), AL::ALValue::array(3, 0.5, 0));
    times[21][13] = 23;
    keys[21][13] = AL::ALValue::array(-0.0137641, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(14);
    keys[22].arraySetSize(14);

    times[22][0] = 0.5;
    keys[22][0] = AL::ALValue::array(0.243948, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[22][1] = 2.5;
    keys[22][1] = AL::ALValue::array(0.297638, AL::ALValue::array(3, -0.666667, -0.0536898), AL::ALValue::array(3, 0.5, 0.0402674));
    times[22][2] = 4;
    keys[22][2] = AL::ALValue::array(1.54938, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[22][3] = 5.5;
    keys[22][3] = AL::ALValue::array(0.964928, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[22][4] = 8.5;
    keys[22][4] = AL::ALValue::array(1.17815, AL::ALValue::array(3, -1, -0.100562), AL::ALValue::array(3, 0.5, 0.0502812));
    times[22][5] = 10;
    keys[22][5] = AL::ALValue::array(1.41746, AL::ALValue::array(3, -0.5, -0.0784895), AL::ALValue::array(3, 0.5, 0.0784895));
    times[22][6] = 11.5;
    keys[22][6] = AL::ALValue::array(1.64909, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[22][7] = 13;
    keys[22][7] = AL::ALValue::array(1.3561, AL::ALValue::array(3, -0.5, 0.103545), AL::ALValue::array(3, 0.5, -0.103545));
    times[22][8] = 14.5;
    keys[22][8] = AL::ALValue::array(1.02782, AL::ALValue::array(3, -0.5, 0.110192), AL::ALValue::array(3, 0.5, -0.110192));
    times[22][9] = 16;
    keys[22][9] = AL::ALValue::array(0.694945, AL::ALValue::array(3, -0.5, 0.0792569), AL::ALValue::array(3, 0.5, -0.0792569));
    times[22][10] = 17.5;
    keys[22][10] = AL::ALValue::array(0.552281, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[22][11] = 19;
    keys[22][11] = AL::ALValue::array(0.871354, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.833333, 0));
    times[22][12] = 21.5;
    keys[22][12] = AL::ALValue::array(0.334454, AL::ALValue::array(3, -0.833333, 0.13071), AL::ALValue::array(3, 0.5, -0.0784257));
    times[22][13] = 23;
    keys[22][13] = AL::ALValue::array(0.243948, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(14);
    keys[23].arraySetSize(14);

    times[23][0] = 0.5;
    keys[23][0] = AL::ALValue::array(1.87766, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[23][1] = 2.5;
    keys[23][1] = AL::ALValue::array(1.86539, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[23][2] = 4;
    keys[23][2] = AL::ALValue::array(1.86693, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[23][3] = 5.5;
    keys[23][3] = AL::ALValue::array(1.86385, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[23][4] = 8.5;
    keys[23][4] = AL::ALValue::array(1.86539, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[23][5] = 10;
    keys[23][5] = AL::ALValue::array(1.8117, AL::ALValue::array(3, -0.5, 0.0135525), AL::ALValue::array(3, 0.5, -0.0135525));
    times[23][6] = 11.5;
    keys[23][6] = AL::ALValue::array(1.78408, AL::ALValue::array(3, -0.5, 0.0276286), AL::ALValue::array(3, 0.5, -0.0276286));
    times[23][7] = 13;
    keys[23][7] = AL::ALValue::array(1.39291, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[23][8] = 14.5;
    keys[23][8] = AL::ALValue::array(1.83624, AL::ALValue::array(3, -0.5, -0.0122697), AL::ALValue::array(3, 0.5, 0.0122697));
    times[23][9] = 16;
    keys[23][9] = AL::ALValue::array(1.84851, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[23][10] = 17.5;
    keys[23][10] = AL::ALValue::array(1.84391, AL::ALValue::array(3, -0.5, 0.00281254), AL::ALValue::array(3, 0.5, -0.00281254));
    times[23][11] = 19;
    keys[23][11] = AL::ALValue::array(1.83164, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.833333, 0));
    times[23][12] = 21.5;
    keys[23][12] = AL::ALValue::array(1.84698, AL::ALValue::array(3, -0.833333, -0.00958796), AL::ALValue::array(3, 0.5, 0.00575278));
    times[23][13] = 23;
    keys[23][13] = AL::ALValue::array(1.87766, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(14);
    keys[24].arraySetSize(14);

    times[24][0] = 0.5;
    keys[24][0] = AL::ALValue::array(-0.208666, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[24][1] = 2.5;
    keys[24][1] = AL::ALValue::array(-0.236277, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[24][2] = 4;
    keys[24][2] = AL::ALValue::array(-0.193327, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[24][3] = 5.5;
    keys[24][3] = AL::ALValue::array(-0.262356, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[24][4] = 8.5;
    keys[24][4] = AL::ALValue::array(-0.262356, AL::ALValue::array(3, -1, 0), AL::ALValue::array(3, 0.5, 0));
    times[24][5] = 10;
    keys[24][5] = AL::ALValue::array(-0.311444, AL::ALValue::array(3, -0.5, 0.0120163), AL::ALValue::array(3, 0.5, -0.0120163));
    times[24][6] = 11.5;
    keys[24][6] = AL::ALValue::array(-0.334454, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[24][7] = 13;
    keys[24][7] = AL::ALValue::array(-0.306841, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[24][8] = 14.5;
    keys[24][8] = AL::ALValue::array(-0.343659, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[24][9] = 16;
    keys[24][9] = AL::ALValue::array(-0.334454, AL::ALValue::array(3, -0.5, -0.00536922), AL::ALValue::array(3, 0.5, 0.00536922));
    times[24][10] = 17.5;
    keys[24][10] = AL::ALValue::array(-0.311444, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[24][11] = 19;
    keys[24][11] = AL::ALValue::array(-0.349794, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.833333, 0));
    times[24][12] = 21.5;
    keys[24][12] = AL::ALValue::array(-0.300706, AL::ALValue::array(3, -0.833333, -0.0294016), AL::ALValue::array(3, 0.5, 0.017641));
    times[24][13] = 23;
    keys[24][13] = AL::ALValue::array(-0.208666, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(14);
    keys[25].arraySetSize(14);

    times[25][0] = 0.5;
    keys[25][0] = AL::ALValue::array(0.343573, AL::ALValue::array(3, -0.166667, 0), AL::ALValue::array(3, 0.666667, 0));
    times[25][1] = 2.5;
    keys[25][1] = AL::ALValue::array(0.276078, AL::ALValue::array(3, -0.666667, 0), AL::ALValue::array(3, 0.5, 0));
    times[25][2] = 4;
    keys[25][2] = AL::ALValue::array(0.314428, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[25][3] = 5.5;
    keys[25][3] = AL::ALValue::array(0.276078, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 1, 0));
    times[25][4] = 8.5;
    keys[25][4] = AL::ALValue::array(0.285283, AL::ALValue::array(3, -1, -0.00272698), AL::ALValue::array(3, 0.5, 0.00136349));
    times[25][5] = 10;
    keys[25][5] = AL::ALValue::array(0.288349, AL::ALValue::array(3, -0.5, -0.00127816), AL::ALValue::array(3, 0.5, 0.00127816));
    times[25][6] = 11.5;
    keys[25][6] = AL::ALValue::array(0.292952, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[25][7] = 13;
    keys[25][7] = AL::ALValue::array(0.246933, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0.5, 0));
    times[25][8] = 14.5;
    keys[25][8] = AL::ALValue::array(0.277612, AL::ALValue::array(3, -0.5, -0.00664737), AL::ALValue::array(3, 0.5, 0.00664737));
    times[25][9] = 16;
    keys[25][9] = AL::ALValue::array(0.286817, AL::ALValue::array(3, -0.5, -0.006136), AL::ALValue::array(3, 0.5, 0.006136));
    times[25][10] = 17.5;
    keys[25][10] = AL::ALValue::array(0.314428, AL::ALValue::array(3, -0.5, -0.00306803), AL::ALValue::array(3, 0.5, 0.00306803));
    times[25][11] = 19;
    keys[25][11] = AL::ALValue::array(0.317496, AL::ALValue::array(3, -0.5, -0.000575305), AL::ALValue::array(3, 0.833333, 0.000958842));
    times[25][12] = 21.5;
    keys[25][12] = AL::ALValue::array(0.31903, AL::ALValue::array(3, -0.833333, -0.00153441), AL::ALValue::array(3, 0.5, 0.000920648));
    times[25][13] = 23;
    keys[25][13] = AL::ALValue::array(0.343573, AL::ALValue::array(3, -0.5, 0), AL::ALValue::array(3, 0, 0));

    try
    {
        //interpolates for all the array entrys
        motion.angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}
//interpolates so that the nao is going to the start position
void climbStairs::goToStartPosition(AL::ALMotionProxy motion){
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(1);
    keys[0].arraySetSize(1);

    times[0][0] = 1;
    keys[0][0] = AL::ALValue::array(-0.196677, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(1);
    keys[1].arraySetSize(1);

    times[1][0] = 1;
    keys[1][0] = AL::ALValue::array(0.0105023, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(1);
    keys[2].arraySetSize(1);

    times[2][0] = 1;
    keys[2][0] = AL::ALValue::array(-0.417919, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(1);
    keys[3].arraySetSize(1);

    times[3][0] = 1;
    keys[3][0] = AL::ALValue::array(0.116264, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(1);
    keys[4].arraySetSize(1);

    times[4][0] = 1;
    keys[4][0] = AL::ALValue::array(-0.27379, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(1);
    keys[5].arraySetSize(1);

    times[5][0] = 1;
    keys[5][0] = AL::ALValue::array(-1.26322, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(1);
    keys[6].arraySetSize(1);

    times[6][0] = 1;
    keys[6][0] = AL::ALValue::array(0.289504, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(1);
    keys[7].arraySetSize(1);

    times[7][0] = 1;
    keys[7][0] = AL::ALValue::array(0.326783, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(1);
    keys[8].arraySetSize(1);

    times[8][0] = 1;
    keys[8][0] = AL::ALValue::array(-0.127895, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(1);
    keys[9].arraySetSize(1);

    times[9][0] = 1;
    keys[9][0] = AL::ALValue::array(-0.0167935, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(1);
    keys[10].arraySetSize(1);

    times[10][0] = 1;
    keys[10][0] = AL::ALValue::array(0.146391, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(1);
    keys[11].arraySetSize(1);

    times[11][0] = 1;
    keys[11][0] = AL::ALValue::array(1.90687, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(1);
    keys[12].arraySetSize(1);

    times[12][0] = 1;
    keys[12][0] = AL::ALValue::array(0.236734, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(1);
    keys[13].arraySetSize(1);

    times[13][0] = 1;
    keys[13][0] = AL::ALValue::array(-0.941081, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(1);
    keys[14].arraySetSize(1);

    times[14][0] = 1;
    keys[14][0] = AL::ALValue::array(-0.452047, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(1);
    keys[15].arraySetSize(1);

    times[15][0] = 1;
    keys[15][0] = AL::ALValue::array(0.0692084, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(1);
    keys[16].arraySetSize(1);

    times[16][0] = 1;
    keys[16][0] = AL::ALValue::array(0.428846, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(1);
    keys[17].arraySetSize(1);

    times[17][0] = 1;
    keys[17][0] = AL::ALValue::array(1.28133, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(1);
    keys[18].arraySetSize(1);

    times[18][0] = 1;
    keys[18][0] = AL::ALValue::array(0.3, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(1);
    keys[19].arraySetSize(1);

    times[19][0] = 1;
    keys[19][0] = AL::ALValue::array(0.266579, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(1);
    keys[20].arraySetSize(1);

    times[20][0] = 1;
    keys[20][0] = AL::ALValue::array(-0.0696426, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(1);
    keys[21].arraySetSize(1);

    times[21][0] = 1;
    keys[21][0] = AL::ALValue::array(-0.0167935, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(1);
    keys[22].arraySetSize(1);

    times[22][0] = 1;
    keys[22][0] = AL::ALValue::array(0.246407, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(1);
    keys[23].arraySetSize(1);

    times[23][0] = 1;
    keys[23][0] = AL::ALValue::array(1.87439, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(1);
    keys[24].arraySetSize(1);

    times[24][0] = 1;
    keys[24][0] = AL::ALValue::array(-0.295337, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(1);
    keys[25].arraySetSize(1);

    times[25][0] = 1;
    keys[25][0] = AL::ALValue::array(0.336033, AL::ALValue::array(3, -0.333333, 0), AL::ALValue::array(3, 0, 0));

    try
    {
        motion.angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }


}
