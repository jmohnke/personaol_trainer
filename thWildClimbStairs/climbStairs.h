// This is start of the header guard.  CLIMBSTAIRS_H can be any unique name.  By convention, we use the name of the header file.
#ifndef CLIMBSTAIRS_H
#define CLIMBSTAIRS_H
#define BOOST_SIGNALS_NO_DEPRECATION_WARNING

#include <boost/shared_ptr.hpp>
#include <alcommon/almodule.h>
#include <string>

#include <alproxies/almotionproxy.h>
#include <alproxies/almemoryproxy.h>
#include <alproxies/altexttospeechproxy.h>
#include <althread/almutex.h>



// This is the content of the .h file, which is where the declarations go
namespace AL {
  class ALBroker;
}

class climbStairs
        : public AL::ALModule
        {

public:
    //Constructor for creation of the module
    climbStairs(
        boost::shared_ptr<AL::ALBroker> broker,
        const std::string& name
    );
    //deconstructor
    virtual ~climbStairs();
    //initialise the values
    virtual void init();
    //the function to be called when the nao button on the back of the head is pressed
    void onRearTactilTouched();
    //the function for walking exactly one stair
    void takeOneStair(AL::ALMotionProxy motion);
    //the function to go to the start position
    void goToStartPosition(AL::ALMotionProxy motion);

private:
    //Memory for Event Handling
    AL::ALMemoryProxy fMemoryProxy;
    //Motion proxy for Interpolation
    AL::ALMotionProxy motion;
    //Callback for events
    boost::shared_ptr<AL::ALMutex> fCallbackMutex;
    //a flag to stop him from calling the event 3 times when only touching him a single time
    bool flag;

};

// This is the end of the header guard
#endif
