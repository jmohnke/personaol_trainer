/**
 * The description of the module.
 */

#ifndef obstacleDetector_obstacleDetector_H
#define obstacleDetector_obstacleDetector_H

#include <boost/shared_ptr.hpp>
#include <alcommon/almodule.h>
#include <string>

#include <alproxies/almemoryproxy.h>
#include <alproxies/altexttospeechproxy.h>
#include <althread/almutex.h>
#include <alproxies/alnavigationproxy.h>
#include <alproxies/almotionproxy.h>

namespace AL
{
  class ALBroker;
}

class ObstacleDetector : public AL::ALModule
{
  public:

    ObstacleDetector(boost::shared_ptr<AL::ALBroker> broker, const std::string& name);

    virtual ~ObstacleDetector();

    /** Overloading ALModule::init().
    * This is called right after the module has been loaded
    */
    virtual void init();


    /**
      * start Navigation mode
      */
    void startModeNavigation() ;
    /**
      * This method will activate the navigation mode : the robot will detect obstacles
      */
    void FrontTactilTouched() ;

    /**
      * this method for choosing to overcome the obstacle by right.
      */
    void goToRight() ;

    /**
      * this method for choosing to overcome the obstacle by left.
      */
    void goToLeft()  ;

    /**
     * this method will be called at the time when the robot detects an obstacle.
     */
    void obstacleDetectedEvent(const std::string eventName, const AL::ALValue position, const std::string subscriberIdentifier) ;

  private:

    //define some variables
    AL::ALMemoryProxy fMemoryProxy;
    AL::ALNavigationProxy navigationProxy ;
    AL::ALMotionProxy motionProxy ;
    AL::ALTextToSpeechProxy fTtsProxy;
    boost::shared_ptr<AL::ALMutex> fCallbackMutex;
    float fState;
};

#endif  // obstacleDetector_obstacleDetector_H
