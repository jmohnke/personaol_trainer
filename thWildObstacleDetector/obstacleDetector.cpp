/**
 * The implementation of the module.
 */

#include "obstacleDetector.h"

#include <alvalue/alvalue.h>
#include <alcommon/alproxy.h>
#include <alcommon/albroker.h>
#include <qi/log.hpp>
#include <althread/alcriticalsection.h>
#include <math.h>

// Attribute declaration
bool hasPermMoveRight = true ;
bool hasPermMoveLeft  = false ;
bool canNavigate = true ;

/**
 * The constructor which sets up the module with its description and methods.
 */
ObstacleDetector::ObstacleDetector (
  boost::shared_ptr<AL::ALBroker> broker,
  const std::string& name): AL::ALModule(broker, name),
    fCallbackMutex(AL::ALMutex::createALMutex())
{
  setModuleDescription("This module presents how to subscribe to a simple Navigation mode with obstacle detecting . ");


  functionName("obstacleDetectedEvent" , getName() , " ");
  BIND_METHOD(ObstacleDetector::obstacleDetectedEvent);

  functionName("FrontTactilTouched" , getName() , " ");
  BIND_METHOD(ObstacleDetector::FrontTactilTouched );



}


/**
 * this method will be called at the time when the robot detects an obstacle.
 */
void ObstacleDetector::obstacleDetectedEvent(const std::string eventName, const AL::ALValue position,const std::string subscriberIdentifier){

    AL::ALCriticalSection section(fCallbackMutex);
    fTtsProxy = AL::ALTextToSpeechProxy(getParentBroker());
    fTtsProxy.say("obstacle detected");

    float obstaclePosX = position[0] ; // obstacle position X
    float obstaclePosY = position[1] ; // obstacle position Y

    if (hasPermMoveRight){
        goToRight(); // choose the right direction
    } else {
        goToLeft(); // choose  the left direction
    }
}

/**
  * this method for choosing to overcome the obstacle by right.
  */
void ObstacleDetector::goToRight(){
    hasPermMoveRight    = false ;
    hasPermMoveLeft     = true  ;

    // Back a 20 cm Back
    motionProxy.moveTo(-0.2 , 0.0 , 0.0 );
    // Change the viewing angle to -45 degrees
    motionProxy.moveTo(0.0  , 0.0 , -M_PI_4);
    // Walk 50 cm it should normally  be sufficient to overcome the obstacle
    motionProxy.moveTo(0.5  , 0.0 , 0.0 );
    //Return to its original angle +45 degrees
    motionProxy.moveTo(0.0  , 0.0 , M_PI_4 );
    // Return to his way
    navigationProxy.moveTo(2.0  , 0.0 , 0.0 );
}

/**
  * this method for choosing to overcome the obstacle by left.
  */
void ObstacleDetector::goToLeft(){
    hasPermMoveRight = true ;
    hasPermMoveLeft = false ;

    // Back a 20 cm Back
    motionProxy.moveTo(-0.2 , 0.0   , 0.0 );
    // Change the viewing angle to +45 degrees
    motionProxy.moveTo(0.0  , 0.0   , M_PI_4);
    // Walk 50 cm it should normally  be sufficient to overcome the obstacle
    motionProxy.moveTo(0.5  , 0.0   , 0.0 );
    //Return to its original angle -45 degrees
    motionProxy.moveTo(0.0  , 0.0   , -M_PI_4 );
    // Return to his way
    navigationProxy.moveTo(2.0   , 0.0  , 0.0 );

}

/**
 * The method being called when unloading the module.
 */
ObstacleDetector::~ObstacleDetector() {
    // unsubscribe the callback methods
  fMemoryProxy.unsubscribeToEvent("obstacleDetectedEvent"   , "ObstacleDetector");
  fMemoryProxy.unsubscribeToEvent("FrontTactilTouched"      , "ObstacleDetector");


}

/**
  * The method being called when loading the module.
  */
void ObstacleDetector::init() {
  try {

    // create a proxy to the memory module
    fMemoryProxy    = AL::ALMemoryProxy(getParentBroker());
    // create a proxy to the Navigation module
    navigationProxy = AL::ALNavigationProxy(getParentBroker());
    // create a proxy to the motion module
    motionProxy     = AL::ALMotionProxy(getParentBroker());


    /** Subscribe to events
    * Arguments:
    * - name of the event
    * - name of the module to be called for the callback
    * - name of the bound method to be called on event
    */
    fMemoryProxy.subscribeToEvent("FrontTactilTouched"                                  ,
                                  "ObstacleDetector" ,
                                  "FrontTactilTouched");
    fMemoryProxy.subscribeToEvent("Navigation/SafeNavigator/DangerousObstacleDetected"  ,
                                  "ObstacleDetector" ,
                                  "obstacleDetectedEvent");

  }
  catch (const AL::ALError& e) {
    qiLogError("module.example") << e.what() << std::endl;
  }
}

/**
  * This method will activate the navigation mode : the robot will detect obstacles
  */
void ObstacleDetector::FrontTactilTouched(){

    if(!canNavigate) {
        return;
    }
    canNavigate = false;

    AL::ALCriticalSection section(fCallbackMutex);

    fTtsProxy = AL::ALTextToSpeechProxy(getParentBroker());
    fTtsProxy.say("Mode obstacle avoidance ");

    startModeNavigation();

}

/**
  * start Navigation mode
  */
void ObstacleDetector::startModeNavigation(){
    // Get into the position wakeup
    motionProxy.wakeUp();
    // Enable arms control by Walk algorithm
    motionProxy.setWalkArmsEnabled(true , true );
    // Configure orthogonal and tangential  security  distance to detect the obstacles
    navigationProxy.setSecurityDistance(0.3); // In this case it Will be 30 cm default 40 cm
    // Set initial value to 5 m allright
    navigationProxy.moveTo(5.0f , 0.0f , 0.0f );
    // set Value canNavigate : True
    canNavigate = true;
}


